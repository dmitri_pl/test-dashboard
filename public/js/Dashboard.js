queue()
    .defer(d3.json, "/api/data2")
    .await(makeGraphs);

function makeGraphs(error, apiData) {

//Start Transformations
	var dataSet = apiData;
	var dateFormat = d3.time.format("%Y-%m-%d %H:%M:%S");
	var binWidth = 20;

	dataSet.forEach(function(d) {
		d.date_posted = dateFormat.parse(d.date_posted);
		d.date_posted_original = d.date_posted;
				d.date_posted.setMinutes(1,1,1);
				//d.date_posted.setDate(1);

		d.percent_power_original = d.percent_power;
		d.percent_power = Math.round(d.percent_power*binWidth/100)/binWidth*100;

		d.total_donations = +1;
	});

	dataSet.sort(function(a, b) { return b.date_posted_original - a.date_posted_original ; });

/*
	mapboxgl.accessToken = 'pk.eyJ1IjoiZGltYS1wbCIsImEiOiJjamR6emY5YmMxbmtzMnFxcDFkdWJiMTc2In0.LFNVEzdEzvXjj2eYmm6fuA';
	var map = new mapboxgl.Map({
	container: 'geomap',
	style: 'mapbox://styles/mapbox/streets-v10'
	});

	var map = L.map('geomap');

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		id: 'dima-pl',
		accessToken: 'pk.eyJ1IjoiZGltYS1wbCIsImEiOiJjamR6emY5YmMxbmtzMnFxcDFkdWJiMTc2In0.LFNVEzdEzvXjj2eYmm6fuA',
		maxZoom: 16
	} ).addTo(map);
*/

	//Create a Crossfilter instance
	var ndx = crossfilter(dataSet);

	function reset_values() {
		consumed = 0;
		powerorigd = 0;
		xd = 0;
		d3.selectAll(".myreddot").classed('myreddot',false);
		dc.renderAll();
		return 1;
	};

	function selected_values(source_group) {
		return {
			all:function () {
				return source_group.all().filter(function(d) {
					return d.value != 0;
				});
			}
		};
	};

	function remove_quasiempty_bins(source_group) {
		return {
			all:function () {
				return source_group.all().filter(function(d) {
					//return Math.abs(d.value) > 0.00001; // if using floating-point numbers
					//return d.value !== 0; // if integers only
					return d.value.split('°')[0].lenght >=2 ? d.value : null;
				});
			}
		};
	};


	//Define Dimensions
	var datePostedOriginal = ndx.dimension(function(d) { return d.date_posted_original; });
	var datePosted = ndx.dimension(function(d) { return d.date_posted; });
	var distanceLevel = ndx.dimension(function(d) { return d.distance_level; });
	var triggerType = ndx.dimension(function(d) { return d.trigger_type ;});
	var ttgroup = ndx.dimension(function(d) { return d.ttgroup ;});
	var profileStatus = ndx.dimension(function(d) { return d.profile_status; });
	var powerLevel = ndx.dimension(function(d) { return d.percent_power; });
	var social = ndx.dimension(function(d) { return d.social_media; });
	var totalDonations  = ndx.dimension(function(d) { return d.total_donations; });
	var supplyConsume  = ndx.dimension(function(d) { return [d.percent_power_original,d.percent_consume]; });



	function reduceAdd(p, v) {
	  ++p.count;
	  p.total += v.value;
	  return p;
	}

	function reduceRemove(p, v) {
	  --p.count;
	  p.total -= v.value;
	  return p;
	}

	function reduceInitial() {
	  return {count: 0, total: 0};
	}
	var ttgroupGroup = ttgroup.group()//.reduce(reduceAdd, reduceRemove, reduceInitial)
	;


	//Calculate metrics
	var projectsByDateOriginal = datePostedOriginal.group();
	var projectsByDate = datePosted.group();
	var profileByDistanceLevel = distanceLevel.group();
	var profileByTriggerType = triggerType.group();
	var profileByStatus = profileStatus.group();
	var projectsByPowerLevel = powerLevel.group();
	var socialGroup = social.group();
	var supplyConsumeGroup = supplyConsume.group();

	var all = ndx.groupAll();

	var filteredProfileByTriggerType = remove_quasiempty_bins(profileByTriggerType);

	//Calculate Groups
	var totalDonationsState = social.group().reduceSum(function(d) {
		return d.total_donations;
	});

	var totalDonationsFundingStatus = profileStatus.group().reduceSum(function(d) {
		return d.funding_status;
	});

	var netTotalDonations = ndx.groupAll().reduceSum(function(d) {return d.total_donations;});

	//Define threshold values for data
	var minDate = datePosted.bottom(1)[0].date_posted;
	var maxDate = datePosted.top(1)[0].date_posted;
	var maxEvents1 = totalDonations.top(1)[0].total_donations;
	var maxEvents2 = projectsByDate.top(1)[0].value;
	var sumall = datePosted.lenght;


	// define lists
	var list = d3.selectAll(".list")
      .data([dataSet]);
	var profilesByDate = datePostedOriginal.top(20);


	var line = d3.svg.line().interpolate('linear');
	var symb = d3.svg.symbol().type('cross').size(function(dd){ return scale(2); });

    //Charts
	var dateChart = dc.lineChart("#date-chart");
	//var gradeLevelChart = dc.rowChart("#grade-chart");
	//var resourceTypeChart = dc.rowChart("#resource-chart");
	var profileStatusChart = dc.pieChart("#status-chart");
	var powerChart = dc.barChart("#power-chart");
	var totalProjects = dc.numberDisplay("#total-projects");
	var netDonations = dc.numberDisplay("#net-donations");
	//var stateDonations = dc.barChart("#state-donations");
	var profileTable = dc.dataTable("#profile-data-table");
	var triggerTable = dc.dataTable("#trigger-data-table");
	var supplyConsumeChart = dc.scatterPlot("#supplyconsume-chart");
	var geoChart = dc.rowChart("#geo-chart");

	var consumed = 0;
	var powerorigd = 0;
	var xd = 0;

	selectField = dc.selectMenu('#menuselect')
        .dimension(social)
        .group(socialGroup);

       dc.dataCount("#row-selection")
        .dimension(ndx)
        .group(all);


	totalProjects
		.formatNumber(d3.format("d"))
		.valueAccessor(function(d){return d; })
		.group(all);

	netDonations
		.formatNumber(d3.format("d"))
		.valueAccessor(function(d){return d; })
		.group(netTotalDonations)
		.formatNumber(d3.format(".3s"));

	dateChart
		//.width(600)
		.height(220)
		.margins({top: 10, right: 50, bottom: 30, left: 50})
		.renderDataPoints({radius: 6, fillOpacity: 0.8, strokeOpacity: 0.8})
		.dimension(datePosted)
		.group(projectsByDate)
		.interpolate("linear")
		//.renderArea(true)
		//.xyTipsOn(true)
		//.renderArea(true)
		.xAxisLabel("Time")
		//.xAxis().ticks(3)
		//.xUnits(function(){return (maxDate - minDate) / 4 ;})
    .xUnits(d3.timeMonths)
		.x(d3.time.scale().domain([minDate, maxDate]))
		.y(d3.scale.linear().domain([1, maxEvents2]))
    chart.renderlet(function (chart) {
   // rotate x-axis labels
    chart.selectAll('g.x text')
     .attr('transform', 'translate(-10,10) rotate(315)');
		//.elasticY(true)
		.renderHorizontalGridLines(true)
    .renderVerticalGridLines(true)
		.transitionDuration(500)
		.brushOn(true)
		//.yAxis().ticks(3)
		;
/*
	dateChart.on('renderlet', function (c) {
			  var thespot;
			  var allDots = c.selectAll('circle.dot');
			  allDots.filter(function(d, i) { //d==datum (obj), i==index (of datapoint on line)
				if (+d.x === testdate1) thespot = i;
			  });
			  // fixed `alldots1` to `allDots1`, now the red point renders correctly
			  allDots.filter((d, i) => i === thespot).classed('myreddot', true);
			  draw_verticals(c, (thespot?[maxdate]:[]));
			})
		;
*/


/*
	resourceTypeChart
        //.width(300)
        .height(220)
        .dimension(resourceType)
        .group(projectsByResourceType)
        .elasticX(true)
        .xAxis().ticks(5);
*/

	triggerTable
        .dimension(datePostedOriginal)
		//.showGroups(false)
		.group(function (d) {
            return d.date_posted_original.toISOString().replace(/T/, ' ').replace(/\..+/, '');
        })
		.size(14)
		.height(220)
		.columns([
			function(d){return d.date_posted_original.toISOString().replace(/T/, ' ').replace(/\..+/, '');},
			function(d){return d.profile_name;},
			function(d){return d.trigger_type;}
			])
		.sortBy(function (d) {return d.ttgroup ? d.ttgroup : Date('2000-11-11') ;})
		.order(d3.descending)
		//.filter(function(d){return d.ttgroup;})
		.on('renderlet', function (table) {
			table.selectAll('.dc-table-group').remove();
			table.selectAll("tbody").filter(":nth-child(even)").selectAll("tr").classed('myevenrow', true);
		})
		;


	profileTable
        .dimension(datePostedOriginal)
		//.showGroups(false)
		.group(function (d) {
            return d.date_posted_original.toISOString().replace(/T/, ' ').replace(/\..+/, '');
        })
		.size(14)
		.height(220)
		.columns([
			function(d){return d.date_posted_original.toISOString().replace(/T/, ' ').replace(/\..+/, '');},
			function(d){return d.profile_name;}])
		.sortBy(function (d) {
            return d.date_posted_original;
        })
		.order(d3.descending)
		.on('renderlet', function (table) {
			table.selectAll('.dc-table-group').remove();
			table.selectAll("tbody").filter(":nth-child(even)").selectAll("tr").classed('myevenrow', true);
			//var tmp = d3.selectAll("tbody").selectAll("tr").selectAll("td")
		})
		;

	profileStatusChart
		.height(220)
		//.width(350)
		.radius(90)
		.innerRadius(40)
		.transitionDuration(1000)
		.dimension(profileStatus)
		.group(profileByStatus);


	powerChart
		//.width(300)
		.height(220)
        .dimension(powerLevel)
        .group(projectsByPowerLevel)
		.brushOn(true)
		.x(d3.scale.linear().domain([0,100]))
		.yAxisLabel("Distribution frequency")
		//.xAxisLabel("Power")
		.xUnits(function(){return binWidth;})
        .xAxis().ticks(4);

	geoChart
		//.width(300)
		.height(220)
		//.margins({top: 10, right: 10, bottom: 40, left: 10})
        .dimension(distanceLevel)
        .group(profileByDistanceLevel)
		//.brushOn(true)
        .elasticX(true)
        .xAxis().ticks(5)
		;


/*
    stateDonations
    	//.width(800)
        .height(220)
        .transitionDuration(1000)
        .dimension(social)
        .group(totalDonationsState)
        .margins({top: 10, right: 50, bottom: 30, left: 50})
        .centerBar(false)
        .gap(5)
        .elasticY(true)
        .x(d3.scale.ordinal().domain(social))
        .xUnits(dc.units.ordinal)
        .renderHorizontalGridLines(true)
        .renderVerticalGridLines(true)
        .ordering(function(d){return d.value;})
        .yAxis().tickFormat(d3.format("s"));
*/

	supplyConsumeChart
        .height(220)
		.x(d3.scale.linear().domain([0, 100]))
		.y(d3.scale.linear().domain([0, 100]))
		.yAxisLabel("Consume")
		//.xAxisLabel("Consume")
		//.clipPadding(10)
		.highlightedSize(4)
		.dimension(supplyConsume)
		//.excludedOpacity(0.5)
		.group(supplyConsumeGroup)
		.brushOn(true)
		.on('renderlet', function (chart) {
			chart.selectAll('path.myreddot').classed('myreddot',false);
			chart.selectAll('path.symbol[transform="translate(' + supplyConsumeChart.x()(powerorigd) + ',' + supplyConsumeChart.y()(consumed ) + ')"]')
				.classed('myreddot',true)
/*				.data(function(d) { return d.key = [powerorigd,consumed] ? d : null ; })
				.enter()
				.append('path')
				.attr('id','redsymbol')
				.attr('class', 'myreddot')
				.attr('d', symb)
				.attr('transform','translate(' + supplyConsumeChart.x()(powerorigd) + ',' + supplyConsumeChart.y()(consumed) + ')')
*/
			//console.log("dima: still alive?" + consumed);
		})
		;






    dc.renderAll();
	function AddXAxis(chartToUpdate, displayText)
	{
		chartToUpdate.svg()
					.append("text")
					.attr("class", "x-axis-label")
					.attr("text-anchor", "middle")
					.attr("x", chartToUpdate.width()/2)
					.attr("y", chartToUpdate.height()-3.5)
					.text(displayText);
	}
	AddXAxis(geoChart, "Distribution frequency");
	AddXAxis(powerChart, "Power");
	AddXAxis(supplyConsumeChart, "Supply");


	d3.selectAll("tbody").selectAll("tr")
	.on("click", function(d,i) {
		//num = document.getElementById("num").value
		//bars(random(num))
		xd = dateChart.x()(d.date_posted);
		console.log("Dima: selected profile = " + d.profile_name + ", def = " + d.date_posted);

		dateChart.g()
			.select('g.chart-body')
			.selectAll('path.horizontal')
			.selectAll('#verticalredline').remove()
		;
		dateChart.render();

		dateChart.g()
			.select('g.chart-body')
			.selectAll('path.horizontal')
			//.selectAll('.myreddot').remove()
			.data([xd]).enter()
			.append('path')
			.attr('id','verticalredline')
			.attr('class', 'horizontal myreddot')
			.attr('d', function(dd){
				var x = xd;
				return line([
					[x, dateChart.y().range()[0]],
					[x, dateChart.y().range()[1]]
				]);
			});

		var statusd = d.profile_status;

		d3.selectAll('g.pie-slice path').classed('myreddot',false);
		d3.selectAll('g.pie-slice').filter(function(dd){return dd.data.key == statusd;}).selectAll('path').classed('myreddot',true);


		var powerd = d.percent_power;

		d3.selectAll('#power-chart rect.bar').classed('myreddot',false);
		d3.selectAll('#power-chart rect.bar').filter(function(dd){return Math.round(dd.data.key * 100/ binWidth) == Math.round(powerd*100/binWidth);}).classed('myreddot',true);

		var distanced = d.distance_level;

		d3.selectAll('#geo-chart rect').classed('myreddot',false);
		d3.selectAll('#geo-chart g.row').filter(function(dd){return dd.key == distanced;}).selectAll('rect').classed('myreddot',true);

		powerorigd = d.percent_power_original;
		consumed = d.percent_consume;

		supplyConsumeChart
			.chartBodyG()
			.selectAll('path.symbol')
			.selectAll('#redsymbol').remove()
		;
		supplyConsumeChart.render();
		AddXAxis(supplyConsumeChart, "Supply");

/*
		var xd = supplyConsumeChart.x()(d.percent_power_original);
		var yd = supplyConsumeChart.y()(d.percent_consume);

		var trd = 'translate(' + supplyConsumeChart.x()(powerorigd) + ',' + supplyConsumeChart.y()(consumed ) + ')';

		d3.select('#supplyconsume-chart path.symbol[transform="translate(' + supplyConsumeChart.x()(powerorigd) + ',' + supplyConsumeChart.y()(consumed ) + ')"]');


		var xd = supplyConsumeChart.x()(d.percent_power_original);
		var yd = supplyConsumeChart.y()(d.percent_consume);

		var xtm = supplyConsumeChart.x()[7.8,11.7764471057884]


		supplyConsumeChart.x()(7.8) --> 12.519
		supplyConsumeChart.y()(11.7764471057884) --> 148.2155688622755

		translate('12.519,148.2155688622755')

		d3.select('circle[cx="'+cxValue+'"]');

		console.log();

		var x0 = +(d3.selectAll('#supplyconsume-chart .chart-body').attr('transform').toString().split(',')[0].split('(')[1]);
		var y0 = +(d3.selectAll('#supplyconsume-chart .chart-body').attr('transform').toString().split(',')[1].split(')')[0]);



		//percent_power":7.8,"percent_consume":11.7764471057884



		supplyConsumeChart
			.chartBodyG()
			.selectAll('path.symbol')//[transform="translate(' + supplyConsumeChart.x()(powerorigd) + ',' + supplyConsumeChart.y()(consumed ) + ')"]')
			.data([{key:[powerorigd,consumed],value:1}])//(function(d) { return d; })
			.enter()
			.append('path')
			.attr('id','redsymbol')
			.attr('class', 'myreddot')
			.attr('d', symb)
			.attr('transform','translate(' + supplyConsumeChart.x()(powerorigd) + ',' + supplyConsumeChart.y()(consumed) + ')')
			;
*/


		/*
		d3.selectAll('#supplyconsume-chart path.symbol').classed('myreddot',false);
		d3.selectAll('#supplyconsume-chart path.symbol').filter(function(dd){
			x0 = x0 +
			console.log("Dima inside " + dd.value.x + ": " + dd.value.y + " == " + xd + ": " + yd);
			return 1;
		});

		console.log("Dima: marking scatterbox value ");
		*/

	});

};
