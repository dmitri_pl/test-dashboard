// var Subjects = require('./models/SubjectViews');
var StaticJson = require('./models/newJson.json');
var StaticApi2Json = require('./models/api2static.json');

const http = require('http');



module.exports = function(app) {

	// server routes ===========================================================
	// handle things like api calls
	// authentication routes	
	// sample api route
 app.get('/api/data', function(req, res) {
  // use mongoose to get all nerds in the database
/*   Subjects.find({}, {'_id': 0, 'school_state': 1, 'resource_type': 1, 'poverty_level': 1, 'date_posted': 1, 'total_donations': 1, 'funding_status': 1, 'grade_level': 1}, function(err, subjectDetails) { */
   // if there is an error retrieving, send the error. 
       // nothing after res.send(err) will execute
/*    if (err) 
   res.send(err); */
    //res.json(subjectDetails); // return all nerds in JSON format
	StaticJson.forEach(function(t){
		t.trigger_type = t.trigger_type == '' ? t.trigger_type : null;
	});

	res.send(StaticJson);
/*   }); */
 });

	app.get('/api/data2static', function(req, res) {
		StaticApi2Json.forEach(function(t){
			t.trigger_type = t.trigger_type == '' ? t.trigger_type : null;
		});
		res.send(StaticApi2Json);
	});

 
// dima: new Jörn's API 
	app.get('/api/data2', function(req, res) {				
		function get() {
			http.get(
				{
					method: 'GET',
					hostname: '116.203.108.232',
					port: 8080,
					path: '/get_dash/',
					agent: false  // create a new agent just for this one request
				}, 
				function(response) {
				//	console.log("dima response status " + response.statusCode);
					response.setEncoding('utf8');
					response.on("error", function(error){
						console.error(error.message);
					});
					let rd = '';
					response.on('data', function (data) {
						rd += data;
					});					
					response.on('end', function () {
						function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
							var R = 6371; // Radius of the earth in km
							var dLat = deg2rad(lat2-lat1);  // deg2rad below
							var dLon = deg2rad(lon2-lon1); 
							var a = 
								Math.sin(dLat/2) * Math.sin(dLat/2) +
								Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
								Math.sin(dLon/2) * Math.sin(dLon/2)
							; 
							var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
							var d = R * c; // Distance in km
							return d;
						};
						function deg2rad(deg) {
							return deg * (Math.PI/180)
						};
						const lat0 = 52.52;
						const long0 = 13.39;
						var tres = (JSON.parse(rd))[0].getDashboardData.likes;
						console.log("dima type : " + typeof tres);
						tres.forEach(function(t){
							t.total_donations = 1;
							t.posts = +(t.posts);
							t.follows = +(t.follows);
							t.followed_by = +(tres.followed_by);
							t.percent_power = 100 * +(t.percent_power);
							t.percent_consume = 100 * +(t.percent_consume);
							t.lat = +(t.lat);
							t.long = +(t.long);
							t.geodistance = getDistanceFromLatLonInKm(lat0,long0,t.lat,t.long);							
							t.lat = t.geodistance >= 5000 ? lat0 + t.lat / 100 : t.lat;
							t.long = t.geodistance >= 5000 ? long0 + t.long / 100 : t.long;
							t.geodistance = getDistanceFromLatLonInKm(lat0,long0,t.lat,t.long);
							t.distance_level = t.geodistance <= 10 ? '<10km' : (t.geodistance <= 100 ? '<100km' : (t.geodistance <= 1000 ? '<1000km' : '1000km & more'));
							t.trigger_type = t.percent_power >= 90 ? 'Influencer ' : null;
							t.trigger_type = t.geodistance <= 10 ? 'Local ' + (t.trigger_type == null ? '' : t.trigger_type) : t.trigger_type;
							t.profile_status = (t.percent_consume >= 90 && t.social_media == 'Historical data') ? 'Private' : t.profile_status;
							t.date_posted = (Date(t.date_posted) >= Date('2018-02-28') && t.social_media == 'Historical data') ? t.date_posted.replace(/2018-02-28/, '2018-02-27') : t.date_posted;
							t.ttgroup = (t.trigger_type == '' || t.trigger_type == null) ? null : t.date_posted;
						});
						console.log("got here... " + JSON.stringify(tres[0]));
						res.send(tres);
					});
			}).end();
			
			
		};
		get();
	});


 // frontend routes =========================================================
 app.get('/', function(req, res) { 
  res.sendFile(__dirname + '/public/index.html');
 });
}
