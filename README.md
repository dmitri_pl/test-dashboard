# test dashboard

<h2>Source code for a dashboard project<h2>

For the original full post please visit: <br/>
<h2>https://anmolkoul.wordpress.com/2015/06/05/interactive-data-visualization-using-d3-js-dc-js-nodejs-and-mongodb</h2>

Required Components:<br/>
D3.js<br/>
Dc.js<br/>
Node.js<br/>
Crossfilter.js<br/>
Jquery<br/>

MongoDB (Dima: this has been carved out and replaced by a static JSON)<br>

Steps for successful execution:<br>
1. Install MongoDB (Dima: not needed anymore) <br>
2.Insert the data into mongoDB as given in the blog (Dima: not needed anymore - instead see sampledata.json) <br>
3.Install Nodejs and NPM<br>
4. Navigate to the installation directory using command prompt and run "npm install", this will install the dependencies<br>
5. Navigate to the installation directory using command prompt and run "npm start"<br>
6. In your browser go to localhost:8080/index.html<br/>
